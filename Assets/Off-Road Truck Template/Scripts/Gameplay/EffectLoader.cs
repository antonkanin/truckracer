﻿using UnityEngine;
using System.Collections;

public class EffectLoader : MonoBehaviour {


	// Amplify Color Help : You need edit script for Amplify Color support


	void Start (){
		
		if (PlayerPrefs.GetInt ("Bloom") == 3) 
			GetComponent<Kino.Bloom> ().enabled = true;
		else
			GetComponent<Kino.Bloom> ().enabled = false;

		// Amplify Color integration
		/*
		if (PlayerPrefs.GetInt ("AmplifyColor") == 3) 
			GetComponent<AmplifyColorEffect> ().enabled = true;
		else 
			GetComponent<AmplifyColorEffect> ().enabled = false;*/

	}
}
