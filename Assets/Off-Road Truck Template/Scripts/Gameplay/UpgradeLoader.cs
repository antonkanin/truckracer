﻿//--------------------------------------------------------------
//
//                    Off-Road Truck Kit
//          Writed by AliyerEdon in fall 2016
//           Contact me : aliyeredon@gmail.com
//
//--------------------------------------------------------------

// This script used for load vehicle upgrades

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UpgradeLoader : MonoBehaviour {

	// Upgrade level list
	public float[] enginePower,maxSpeed,fuelUpgrade;

	VehicleController2017 truck;

	ItemManager manager;

	public GameObject rain;

	void Start () {


		if (SceneManager.GetActiveScene ().name.Contains ("Garage") ||
		    SceneManager.GetActiveScene ().name.Contains ("Menu")) {
			rain.SetActive (false);


			return;
		}
		
		truck = GetComponent<VehicleController2017> ();
		manager = GameObject.FindObjectOfType<ItemManager> ();

		// Set truck motor power based on upgrade value on upgrade menu
		truck.enginePower = enginePower[PlayerPrefs.GetInt("Engine"+PlayerPrefs.GetInt("TruckID").ToString())];
		truck.maxSpeed = maxSpeed[PlayerPrefs.GetInt("Speed"+PlayerPrefs.GetInt("TruckID").ToString())];

		// Load fuel timer interval based on upgrade value on upgrade menu
		manager.FualInterval = fuelUpgrade[PlayerPrefs.GetInt("Fuel"+PlayerPrefs.GetInt("TruckID").ToString())];

	}

}
