﻿//--------------------------------------------------------------
//
//                    Off-Road Truck Kit
//          Writed by AliyerEdon in fall 2016
//           Contact me : aliyeredon@gmail.com
//
//--------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class ItemTrigger : MonoBehaviour {

	// Use this for initialization
	public ItemManager manager;

	bool alarmed;


	void OnTriggerEnter (Collider col) {
	

		if (col.CompareTag("Item")) {

			if (col.GetComponent<Item> ().taken == false) {
				col.GetComponent<Item> ().taken = true;
				manager.AddScore(col.GetComponent<Item>().score);
				col.GetComponent<Item> ().StartCoroutine ("ReScore");
			}
		}

		if (col.CompareTag("Player")) {
			manager.TotalFuel = 101;
			manager.Alarm.Play ();
		}
	}
	
	// Update is called once per frame
	void Start () {
		if (!manager)
			manager = GameObject.FindObjectOfType<ItemManager> ();
	}



}
